#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	char symbol;
	int num1;
	int num2;

	cout << "Enter operator: +, -, *, /: ";
	cin >> symbol;

	cout << "Enter any integers: ";
	cin >> num1;

	cout << "Enter any integer: ";
	cin >> num2;

	if (symbol == '+') {
		cout << num1 << " + " << num2 << " = " << num1 + num2;
	}
	else if (symbol == '-') {
		cout << num1 << " - " << num2 << " = " << num1 - num2;
	}
	else if (symbol == '*') {
		cout << num1 << " * " << num2 << " = " << num1 * num2;
	}
	else if (symbol == '/') {
		cout << num1 << " / " << num2 << " = " << num1 / num2;
	}
	else {
		cout << "Please enter an appropriate symbol.\n";
	}
	


	(void)_getch();
	return 0;
}